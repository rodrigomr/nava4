* SPICE NETLIST
***************************************

.SUBCKT Q C B E S G
.ENDS
***************************************
.SUBCKT lddn D G S B
.ENDS
***************************************
.SUBCKT probepad pad
.ENDS
***************************************
.SUBCKT L POS NEG SUB
.ENDS
***************************************
.SUBCKT hall A B C D S P1 P2
.ENDS
***************************************
.SUBCKT inver
** N=4 EP=0 IP=0 FDC=2
M0 Vout Vin Vss Vss nmos4 L=3.5e-07 W=3e-06 AD=2.55e-12 AS=2.55e-12 PD=4.7e-06 PS=4.7e-06 NRD=0.141667 NRS=0.141667 $X=-550 $Y=-2200 $D=22
M1 Vout Vin Vdd Vdd pmos4 L=3.5e-07 W=9e-06 AD=7.65e-12 AS=7.65e-12 PD=1.07e-05 PS=1.07e-05 NRD=0.0472222 NRS=0.0472222 $X=-550 $Y=4475 $D=26
.ENDS
***************************************
