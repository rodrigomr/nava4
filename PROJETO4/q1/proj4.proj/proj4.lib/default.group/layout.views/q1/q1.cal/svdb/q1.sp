* SPICE NETLIST
***************************************

.SUBCKT Q C B E S G
.ENDS
***************************************
.SUBCKT lddn D G S B
.ENDS
***************************************
.SUBCKT probepad pad
.ENDS
***************************************
.SUBCKT L POS NEG SUB
.ENDS
***************************************
.SUBCKT hall A B C D S P1 P2
.ENDS
***************************************
.SUBCKT q1
** N=4 EP=0 IP=0 FDC=2
M0 Vout Vin Vss Vss nmos4 L=3.5e-07 W=2.5e-06 AD=2.125e-12 AS=2.375e-12 PD=4.2e-06 PS=4.4e-06 NRD=0.17 NRS=0.17 $X=2200 $Y=-3475 $D=22
M1 Vout Vin Vdd Vdd pmos4 L=3.5e-07 W=7.5e-06 AD=6.375e-12 AS=7.125e-12 PD=9.2e-06 PS=9.4e-06 NRD=0.0566667 NRS=0.0566667 $X=2200 $Y=1425 $D=26
.ENDS
***************************************
