* SPICE NETLIST
***************************************

.SUBCKT Q C B E S G
.ENDS
***************************************
.SUBCKT lddn D G S B
.ENDS
***************************************
.SUBCKT probepad pad
.ENDS
***************************************
.SUBCKT L POS NEG SUB
.ENDS
***************************************
.SUBCKT hall A B C D S P1 P2
.ENDS
***************************************
.SUBCKT q14
** N=7 EP=0 IP=0 FDC=10
M0 Vb Va Vss Vss nmos4 L=3.5e-07 W=2.5e-06 AD=2.125e-12 AS=2.375e-12 PD=4.2e-06 PS=4.4e-06 NRD=0.17 NRS=0.17 $X=7525 $Y=-5500 $D=22
M1 Vc Vb Vss Vss nmos4 L=3.5e-07 W=2.5e-06 AD=2.125e-12 AS=2.375e-12 PD=4.2e-06 PS=4.4e-06 NRD=0.17 NRS=0.17 $X=14025 $Y=-5500 $D=22
M2 Vd Vc Vss Vss nmos4 L=3.5e-07 W=2.5e-06 AD=2.125e-12 AS=2.375e-12 PD=4.2e-06 PS=4.4e-06 NRD=0.17 NRS=0.17 $X=20525 $Y=-5500 $D=22
M3 Ve Vd Vss Vss nmos4 L=3.5e-07 W=2.5e-06 AD=2.125e-12 AS=2.375e-12 PD=4.2e-06 PS=4.4e-06 NRD=0.17 NRS=0.17 $X=27025 $Y=-5500 $D=22
M4 Va Ve Vss Vss nmos4 L=3.5e-07 W=2.5e-06 AD=2.125e-12 AS=2.375e-12 PD=4.2e-06 PS=4.4e-06 NRD=0.17 NRS=0.17 $X=33525 $Y=-5500 $D=22
M5 Vb Va Vdd Vdd pmos4 L=3.5e-07 W=7.5e-06 AD=6.375e-12 AS=7.125e-12 PD=9.2e-06 PS=9.4e-06 NRD=0.0566667 NRS=0.0566667 $X=7525 $Y=-600 $D=26
M6 Vc Vb Vdd Vdd pmos4 L=3.5e-07 W=7.5e-06 AD=6.375e-12 AS=7.125e-12 PD=9.2e-06 PS=9.4e-06 NRD=0.0566667 NRS=0.0566667 $X=14025 $Y=-600 $D=26
M7 Vd Vc Vdd Vdd pmos4 L=3.5e-07 W=7.5e-06 AD=6.375e-12 AS=7.125e-12 PD=9.2e-06 PS=9.4e-06 NRD=0.0566667 NRS=0.0566667 $X=20525 $Y=-600 $D=26
M8 Ve Vd Vdd Vdd pmos4 L=3.5e-07 W=7.5e-06 AD=6.375e-12 AS=7.125e-12 PD=9.2e-06 PS=9.4e-06 NRD=0.0566667 NRS=0.0566667 $X=27025 $Y=-600 $D=26
M9 Va Ve Vdd Vdd pmos4 L=3.5e-07 W=7.5e-06 AD=6.375e-12 AS=7.125e-12 PD=9.2e-06 PS=9.4e-06 NRD=0.0566667 NRS=0.0566667 $X=33525 $Y=-600 $D=26
.ENDS
***************************************
