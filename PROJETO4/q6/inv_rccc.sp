* File: inv_rccc
* Created: Tue Jun 16 11:00:09 2015
* Program "Calibre xRC"
* Version "v2006.2_16.16"
* 
.include "inv_rccc.pex"
.subckt INV N_VIN_M0_g N_VOUT_M0_d N_VDD_M1_s N_VSS_M0_s
* 
mM0 N_VOUT_M0_d N_VIN_M0_g N_VSS_M0_s N_VSS_M0_b MODN L=3.5e-07 W=2.5e-06
+ AD=2.125e-12 AS=2.375e-12 PD=4.2e-06 PS=4.4e-06 NRD=0.17 NRS=0.17
mM1 N_VOUT_M1_d N_VIN_M1_g N_VDD_M1_s N_VDD_M1_b MODP L=3.5e-07 W=7.5e-06
+ AD=6.375e-12 AS=7.125e-12 PD=9.2e-06 PS=9.4e-06 NRD=0.0566667 NRS=0.0566667
*
.include "inv_rccc.Q1.pxi"
*
.ends
*
*
